import co.dtub.cse360.ipacs.data.Doctor;
import co.dtub.cse360.ipacs.data.Patient;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static  org.junit.Assert.*;

public class DoctorTest {

    @Test
    public void test_addPatient() {
        Doctor tempDoctor = new Doctor("Mr.", "John", "Doe", "j.doe@gmail.com", "crazyeggs");
        Patient tempPatient1 = new Patient("Mrs.", "Jane", "Redding", "j.red@gmail.com", "coolpanda");
        Patient tempPatient2 = new Patient("Mr.", "Chris", "Redding", "c.red@gmail.com", "sleepyflake");

        Set<Patient> temp_patients1 = new HashSet<>();
        Set<Patient> temp_patients2 = new HashSet<>();
        tempDoctor.addPatient(tempPatient1, tempPatient2);

        temp_patients1.add(tempPatient1);
        temp_patients1.add(tempPatient2);


        temp_patients2 = tempDoctor.getPatients();
        assertEquals(temp_patients1, temp_patients2);
    }

    @Test
    public void test_removePatient() {
        Doctor tempDoctor = new Doctor("Mr.", "John", "Doe", "j.doe@gmail.com", "crazyeggs");
        Patient tempPatient1 = new Patient("Mrs.", "Jane", "Redding", "j.red@gmail.com", "coolpanda");
        Patient tempPatient2 = new Patient("Mr.", "Chris", "Redding", "c.red@gmail.com", "sleepyflake");

        Set<Patient> temp_patients1 = new HashSet<>();
        Set<Patient> temp_patients2 = new HashSet<>();
        tempDoctor.addPatient(tempPatient1, tempPatient2);

        temp_patients1.add(tempPatient1);

        tempDoctor.removePatient(tempPatient2);

        temp_patients2 = tempDoctor.getPatients();
        assertEquals(temp_patients1, temp_patients2);
    }
}
