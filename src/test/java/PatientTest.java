import co.dtub.cse360.ipacs.data.Patient;

import org.junit.Test;

import java.util.Map;

import static  org.junit.Assert.*;

public class PatientTest {

    @Test
    public void test_putSymptoms() {
        Patient tempPatient1 = new Patient("Mrs.", "Jane", "Redding", "j.red@gmail.com", "coolpanda");
        Map<String, Integer> temp_symptoms1 = Patient.DEFAULT_SYMPTOMS;
        Map<String, Integer> temp_symptoms2;

        temp_symptoms1.put("Pain", 3);
        temp_symptoms1.put("Tiredness", 4);
        tempPatient1.putSymptom("Pain", 3);
        tempPatient1.putSymptom("Tiredness", 4);

        temp_symptoms2 = tempPatient1.getSymptoms();
        assertTrue(temp_symptoms1.equals(temp_symptoms2));
    }
}
