import co.dtub.cse360.ipacs.data.Admin;

import org.junit.Test;

import static  org.junit.Assert.*;

public class UserTest {

    @Test
    public void test_getFullName() {
        Admin tempUser = new Admin("Mr.", "John", "Doe","j.doe@gmail.com", "crazyeggs");
        String result = tempUser.getFullName();

        assertEquals("Mr. John Doe", result);
    }

    @Test
    public void test_checkPassword() {
        Admin tempUser = new Admin("Mr.", "John", "Doe","j.doe@gmail.com", "crazyeggs");
        boolean result = tempUser.checkPassword("crazyeggs");

        assertEquals(true, result);
    }
}
