import co.dtub.cse360.ipacs.data.Admin;

import org.junit.Test;

import static  org.junit.Assert.*;

public class AdminTest {

    @Test
    public void test_isSuperFalse() {
        Admin tempUser = new Admin("Mr.", "John", "Doe","j.doe@gmail.com", "crazyeggs");
        boolean result = tempUser.isSuper();

        assertEquals(false, result);
    }

    @Test
    public void test_isSuperTrue() {
        Admin tempUser = new Admin("j.doe@gmail.com", "crazyeggs");
        boolean result = tempUser.isSuper();

        assertEquals(true, result);
    }
}
