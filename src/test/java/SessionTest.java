import co.dtub.cse360.ipacs.data.Doctor;
import co.dtub.cse360.ipacs.data.Session;

import java.util.concurrent.TimeUnit;

import static  org.junit.Assert.*;

public class SessionTest {
    Doctor tempDoctor = new Doctor("Mr.", "John", "Doe", "j.doe@gmail.com", "crazyeggs");
    Session tempSession = new Session(tempDoctor, 1);

    public void test_check(){
        boolean result1 = tempSession.check();
        try {
            TimeUnit.MINUTES.sleep(2);
        }
        catch (InterruptedException e){
            System.err.println("InterruptedException: " + e.getMessage());
        }

        boolean result2 = tempSession.check();

        assertEquals(false, result1);
        assertEquals(true, result2);

    }
}
