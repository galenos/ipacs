package co.dtub.cse360.ipacs.data;

import java.util.Set;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

public class DoctorSpecs extends GenericUserSpecs<Doctor> {

    public static Specification<Doctor> patient(Patient patient) {
        return (Root<Doctor> root, CriteriaQuery<?> query, CriteriaBuilder cb) ->
                cb.isMember(patient, root.<Set<Patient>> get("patients"));
    }
}
