package co.dtub.cse360.ipacs.data;

import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class AdminSpecs extends GenericUserSpecs<Admin> {

    public static Specification<Admin> isSuper() {
        return (Root<Admin> root, CriteriaQuery<?> query, CriteriaBuilder cb) ->
                cb.isTrue(root.get("superAdmin"));
    }
}
