package co.dtub.cse360.ipacs.data;

public class AlertMessage {

    private String content;
    private String id;

    public AlertMessage() {}

    public AlertMessage(String id, String content) {
        this.id = id;
        this.content = content;
    }

    public String getId() {
		return id;
	}

    public void setId(String id) {
		this.id = id;
	}

    public String getContent() {
        return content;
    }
 
    public void setContent(String content) {
    	this.content = content;
    }
}
