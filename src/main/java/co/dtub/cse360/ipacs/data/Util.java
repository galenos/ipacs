package co.dtub.cse360.ipacs.data;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Base64;

public class Util {

    private static SecureRandom random = new SecureRandom();

    public static String newId() {
        return new BigInteger(260, random).toString(32);
    }
}
