package co.dtub.cse360.ipacs.data;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends GenericUserRepository<User> {

    @Query(value="select u from User u where u.email = ?1 or u.id = ?1")
    User findOne(String emailOrId);
}
