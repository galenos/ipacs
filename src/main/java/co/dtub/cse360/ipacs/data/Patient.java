package co.dtub.cse360.ipacs.data;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name="PATIENT")
public class Patient extends User {

    @Transient
    public static final Map<String, Integer> DEFAULT_SYMPTOMS;
    static {
        DEFAULT_SYMPTOMS = new HashMap<>();
        DEFAULT_SYMPTOMS.put("Pain", 0);
        DEFAULT_SYMPTOMS.put("Drowsiness", 0);
        DEFAULT_SYMPTOMS.put("Nausea", 0);
        DEFAULT_SYMPTOMS.put("Anxiety", 0);
        DEFAULT_SYMPTOMS.put("Depression", 0);
    }

    @ManyToMany(mappedBy="patients", fetch=FetchType.LAZY)
    private Set<Doctor> doctors = new HashSet<>();

    @ElementCollection(fetch=FetchType.EAGER)
    @CollectionTable(name="PATIENT_SYMPTOM", joinColumns=@JoinColumn(name="patient_id"))
    @MapKeyColumn(name="symptom_type")
    @Column(name="severity")
    private Map<String, Integer> symptoms = new HashMap<>();

    @Column(name="notes")
    private String notes;

    public Patient() {}
    public Patient(
            String title,
            String firstName,
            String lastName,
            String email,
            String password) {
        super(UserRole.PATIENT, title, firstName, lastName, email, password);
        putSymptoms(DEFAULT_SYMPTOMS);
    }

    public Set<Doctor> getDoctors() {
        return this.doctors;
    }

    public int getSymptom(String name) {
        return this.symptoms.get(name);
    }
    public Map<String, Integer> getSymptoms() {
        return this.symptoms;
    }

    public void putSymptom(String symptom, int severity) {
        this.symptoms.put(symptom, severity);
    }
    public void putSymptoms(Map<String, Integer> symptoms) {
        this.symptoms.putAll(symptoms);
    }

    public String getSevereSymptoms() {

        Set<String> result = new HashSet<>();

        int total = symptoms.size();
        double mean = 0, variance = 0, threshold, stddev = 0;
        
        for (int s : symptoms.values()) mean += s;
        mean /= total;
        for (int s : symptoms.values()) variance += Math.pow(mean - s, 2);
        variance /= total;
        
        stddev = Math.pow(variance, 0.5);

        threshold = mean + 1.5 * stddev;

        for (Map.Entry<String, Integer> s : symptoms.entrySet()) {
            int v = s.getValue();
            if (v > 8 || v > threshold) result.add(s.getKey());
        }

        String resultString = result.toString();
        return resultString.substring(1, resultString.length() - 1);
    }

    public void removeSymptom(String... symptoms) {
        for (String s : symptoms) {
            this.symptoms.remove(s);
        }
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
    public String getNotes() {
        return notes;
    }
}
