package co.dtub.cse360.ipacs.data;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name="DOCTOR")
public class Doctor extends User {

    @ManyToMany(fetch=FetchType.LAZY)
    @LazyCollection(LazyCollectionOption.EXTRA)
    @JoinTable(name="DOCTOR_PATIENT",
            joinColumns=@JoinColumn(name="doctor_id"),
            inverseJoinColumns=@JoinColumn(name="patient_id"))
    private Set<Patient> patients = new HashSet<>();

    @Column(name="qualifications")
    private String qualifications;

    @Column(name="notes")
    private String notes;

    public Doctor() {}
    public Doctor(
            String title,
            String firstName,
            String lastName,
            String email,
            String password) {
        super(UserRole.DOCTOR, title, firstName, lastName, email, password);
    }

    public Set<Patient> getPatients() {
        return this.patients;
    }
    public void addPatient(Patient... patients) {
        Collections.addAll(this.patients, patients);
    }
    public void removePatient(Patient... patients) {
        for (Patient patient : patients) {
            this.patients.remove(patient);
        }
    }

    public String getQualifications() {
        return qualifications;
    }
    public void setQualifications(String qualifications) {
        this.qualifications = qualifications;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
    public String getNotes() {
        return notes;
    }
}
