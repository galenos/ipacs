package co.dtub.cse360.ipacs.data;

import co.dtub.cse360.ipacs.form.LoginForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

@Service
public class UserService {

    private static final String SESSION_COOKIE = "sessionId";

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private SessionRepository sessionRepository;

    public boolean exists(String email) {
        return userRepository.findOne(email) != null;
    }

    @Transactional
    public Session login(HttpServletRequest request, HttpServletResponse response) {
        Cookie cookies[] = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(SESSION_COOKIE)) {
                    Session session = sessionRepository.findOne(cookie.getValue());
                    if (session != null) {
                        if (session.check()) {
                            return session;
                        }
                        sessionRepository.delete(session);
                    }
                    cookie.setMaxAge(0);
                    response.addCookie(cookie);
                }
            }
        }
        return null;
    }

    public <T extends User> T login(Class<T> userClass, HttpServletRequest request, HttpServletResponse response) {
        Session session = login(request, response);
        if (session != null && userClass.isInstance(session.getUser())) {
            return userClass.cast(session.getUser());
        }
        return null;
    }

    @Transactional
    public Session login(LoginForm loginForm, HttpServletResponse response) {
        User user = userRepository.findOne(loginForm.getEmail());
        if (user != null && user.isActive() && user.checkPassword(loginForm.getPassword())) {
            Session session = new Session(user, 60);
            sessionRepository.saveAndFlush(session);
            response.addCookie(new Cookie(SESSION_COOKIE, session.getId()));
            return session;
        } else {
            return null;
        }
    }

    @Transactional
    public Session login(User user, HttpServletResponse response) {
        Session session = new Session(user, 60);
        sessionRepository.saveAndFlush(session);
        response.addCookie(new Cookie(SESSION_COOKIE, session.getId()));
        return session;
    }

    @Transactional
    public void logout(HttpServletRequest request, HttpServletResponse response) {
        Cookie cookies[] = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(SESSION_COOKIE)) {
                    Session session = sessionRepository.findOne(cookie.getValue());
                    if (session != null) {
                        sessionRepository.delete(session);
                    }
                    cookie.setMaxAge(0);
                    response.addCookie(cookie);
                }
            }
        }
    }
}
