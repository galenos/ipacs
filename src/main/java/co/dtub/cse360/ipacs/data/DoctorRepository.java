package co.dtub.cse360.ipacs.data;

import org.springframework.stereotype.Repository;

@Repository
public interface DoctorRepository extends GenericUserRepository<Doctor> {

}
