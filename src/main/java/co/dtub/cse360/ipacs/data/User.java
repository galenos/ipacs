package co.dtub.cse360.ipacs.data;

import javax.persistence.*;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Calendar;

@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@SecondaryTable(name="USER_PROFILE", pkJoinColumns=@PrimaryKeyJoinColumn(name="id"))
public abstract class User {

    @Id
    @Column(name="id")
    private String id;

    @Column(name="role")
    @Enumerated(value=EnumType.STRING)
    private UserRole role;

    @Column(name="title")
    private String title;
    @Column(name="first_name")
    private String firstName;
    @Column(name="last_name", table="USER_PROFILE")
    private String lastName;

    @Column(name="email", unique=true)
    private String email;

    @Column(name="password")
    private String password;

    @Column(name="enabled")
    private boolean enabled;

    @Column(name="address", table="USER_PROFILE")
    private String address;
    @Column(name="city", table="USER_PROFILE")
    private String city;
    @Column(name="state", table="USER_PROFILE")
    private String state;
    @Column(name="zip", table="USER_PROFILE")
    private String zip;

    @Column(name="primary_phone", table="USER_PROFILE")
    private String primaryPhone;
    @Column(name="secondary_phone", table="USER_PROFILE")
    private String secondaryPhone;

    @Column(name="gender", table="USER_PROFILE")
    private String gender;

    @Column(name="birth_date", table="USER_PROFILE")
    @Temporal(TemporalType.DATE)
    private Calendar birthDate;

    protected User() {}
    protected User(
            UserRole role,
            String title,
            String firstName,
            String lastName,
            String email,
            String password) {
        this.id = Util.newId();
        this.role = role;
        this.setTitle(title);
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setEmail(email);
        this.setPassword(password);
        this.enabled = true;
    }

    public String getId() {
        return this.id;
    }

    public UserRole getRole() {
        return this.role;
    }

    public String getFullName() {
        String result = "";
        if (this.title != null && !this.title.isEmpty()) {
            result += this.title + ' ';
        }
        return result + this.firstName + ' ' + this.lastName;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getFirstName() {
        return this.firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return this.lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return this.email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public boolean checkPassword(String password) {
        return new BCryptPasswordEncoder().matches(password, this.password);
    }
    public void setPassword(String password) {
        this.password = new BCryptPasswordEncoder().encode(password);
    }

    public boolean isActive() {
        return this.enabled;
    }
    public void activate() {
        this.enabled = true;
    }
    public void deactivate() {
        this.enabled = false;
    }

    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }
    public String getZip() {
        return zip;
    }
    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getPrimaryPhone() {
        return primaryPhone;
    }
    public void setPrimaryPhone(String primaryPhone) {
        this.primaryPhone = primaryPhone;
    }
    public String getSecondaryPhone() {
        return secondaryPhone;
    }
    public void setSecondaryPhone(String secondaryPhone) {
        this.secondaryPhone = secondaryPhone;
    }

    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }

    public Calendar getBirthDate() {
        return birthDate;
    }
    public void setBirthDate(Calendar birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof User && this.id.equals(((User) obj).id);
    }
}
