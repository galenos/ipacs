package co.dtub.cse360.ipacs.data;

import org.springframework.stereotype.Repository;

@Repository
public interface PatientRepository extends GenericUserRepository<Patient> {

}
