package co.dtub.cse360.ipacs.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="SESSION")
public class Session {

    @Id
    @Column(name="id")
    private String id;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="user_id")
    User user;

    @Column(name="last_activity")
    long lastActivity;

    @Column(name="timeout")
    long timeout;

    @Column(name="valid")
    boolean valid;

    Session() {}
    public Session(
            User user,
            int timeout) {
        this.id = Util.newId();
        this.user = user;
        this.lastActivity = System.currentTimeMillis();
        this.timeout = timeout * 60000; // Timeout in minutes
        this.valid = true;
    }

    public String getId() {
        return this.id;
    }

    public UserRole getUserRole() {
        return this.user.getRole();
    }

    public User getUser() {
        return this.user;
    }

    public boolean check() {
        long now = System.currentTimeMillis();
        if (this.valid && now - this.lastActivity < this.timeout) {
            this.lastActivity = now;
            return true;
        }
        else {
            this.valid = false;
            return false;
        }
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Session && this.id.equals(((Session) obj).id);
    }
}
