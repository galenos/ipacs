package co.dtub.cse360.ipacs.data;

public enum UserRole {
    ADMIN,
    DOCTOR,
    PATIENT
}
