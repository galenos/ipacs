package co.dtub.cse360.ipacs.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
interface GenericUserRepository<U extends User> extends JpaRepository<U, String>, JpaSpecificationExecutor<U> {

}
