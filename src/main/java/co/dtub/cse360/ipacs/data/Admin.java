package co.dtub.cse360.ipacs.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="ADMIN")
public class Admin extends User {

    @Column(name="super")
    private boolean superAdmin;

    public Admin() {}
    public Admin(
            String email,
            String password) {
        super(UserRole.ADMIN, null, "Super", "Admin", email, password);
        this.superAdmin = true;
    }
    public Admin(
            String title,
            String firstName,
            String lastName,
            String email,
            String password) {
        super(UserRole.ADMIN, title, firstName, lastName, email, password);
        this.superAdmin = false;
    }

    public boolean isSuper() {
        return this.superAdmin;
    }

    @Override
    public void deactivate() {
        if (!this.superAdmin) {
            super.deactivate();
        }
    }
}
