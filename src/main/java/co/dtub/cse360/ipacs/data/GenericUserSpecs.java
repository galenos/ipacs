package co.dtub.cse360.ipacs.data;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

class GenericUserSpecs<U extends User> {

    public static <U> Specification<U> firstName(String firstName) {
        return (Root<U> root, CriteriaQuery<?> query, CriteriaBuilder cb) ->
                cb.like(root.<String> get("firstName"), firstName + "%");
    }

    public static <U> Specification<U> lastName(String lastName) {
        return (Root<U> root, CriteriaQuery<?> query, CriteriaBuilder cb) ->
                cb.like(root.<String> get("firstName"), lastName + "%");
    }

    public static <U> Specification<U> email(String email) {
        return (Root<U> root, CriteriaQuery<?> query, CriteriaBuilder cb) ->
                cb.like(root.<String> get("email"), email + "%");
    }
}
