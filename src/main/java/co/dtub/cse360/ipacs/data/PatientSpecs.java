package co.dtub.cse360.ipacs.data;

import java.util.Set;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

public class PatientSpecs extends GenericUserSpecs<Patient> {

    public static Specification<Patient> doctor(Doctor doctor) {
        return (Root<Patient> root, CriteriaQuery<?> query, CriteriaBuilder cb) ->
                cb.isMember(doctor, root.<Set<Doctor>> get("doctors"));
    }
}
