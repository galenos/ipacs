package co.dtub.cse360.ipacs;

import co.dtub.cse360.ipacs.data.*;
import co.dtub.cse360.ipacs.form.SignupForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

@Controller
public class PatientSignup {

    @Autowired
    UserService userService;
    @Autowired
    UserRepository userRepository;

    // Patient sign up page
    @RequestMapping(value="/signup", method=RequestMethod.GET)
    public String patientSignupForm(
            Model model,
            HttpServletRequest request,
            HttpServletResponse response) {

        Session session = userService.login(request, response);
        if (session != null) {
            return "redirect:/";
        }

        model.addAttribute("title", "Patient Signup");
        model.addAttribute("signupForm", new SignupForm("/signup"));
        return "signup";
    }

    @Transactional
    @RequestMapping(value="/signup", method=RequestMethod.POST)
    public String patientSignup(
            @ModelAttribute("signupForm") SignupForm signupForm,
            Model model,
            HttpServletRequest request,
            HttpServletResponse response) {

        Session session = userService.login(request, response);
        if (session != null) {
            return "redirect:/";
        }

        String message = signupForm.validate();
        if (!message.isEmpty()) {
            model.addAttribute("title", "Patient Signup");
            model.addAttribute("formStatus", message);
            return "signup";
        }

        if (userService.exists(signupForm.getEmail())) {
            model.addAttribute("title", "Patient Signup");
            model.addAttribute("formStatus", "Email address is already in use");
            return "signup";
        }

        Patient patient = signupForm.getPatient();
        userRepository.saveAndFlush(patient);
        userService.login(patient, response);
        return "redirect:/profile";
    }

    // Create patient from doctor view
    @RequestMapping(value="/patients/new", method=RequestMethod.GET)
    public String addPatientForm(
            Model model,
            HttpServletRequest request,
            HttpServletResponse response) {

        Session session = userService.login(request, response);
        if (session == null || session.getUserRole() != UserRole.DOCTOR) {
            return "redirect:/";
        }

        model.addAttribute("title", "New Patient");
        model.addAttribute("signupForm", new SignupForm("/patients/new"));
        model.addAttribute("user", session.getUser());
        return "signup";
    }

    @Transactional
    @RequestMapping(value="/patients/new", method=RequestMethod.POST)
    public String addPatient(
            @ModelAttribute("signupForm") SignupForm signupForm,
            Model model,
            HttpServletRequest request,
            HttpServletResponse response) {

        Session session = userService.login(request, response);
        if (session == null || session.getUserRole() != UserRole.DOCTOR) {
            return "redirect:/";
        }

        String message = signupForm.validate();
        if (!message.isEmpty()) {
            model.addAttribute("title", "New Patient");
            model.addAttribute("user", session.getUser());
            model.addAttribute("formStatus", message);
            return "signup";
        }

        if (userService.exists(signupForm.getEmail())) {
            model.addAttribute("title", "New Patient");
            model.addAttribute("user", session.getUser());
            model.addAttribute("formStatus", "Email address is already in use");
            return "signup";
        }

        Patient patient = signupForm.getPatient();
        userRepository.saveAndFlush(patient);
        return "redirect:/patients/" + patient.getId();
    }
}
