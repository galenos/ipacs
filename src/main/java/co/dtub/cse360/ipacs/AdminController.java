package co.dtub.cse360.ipacs;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import co.dtub.cse360.ipacs.data.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class AdminController {

    @Autowired
    private UserService userService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AdminRepository adminRepository;
    @Autowired
    private DoctorRepository doctorRepository;

    @RequestMapping(value="/admin", method=RequestMethod.GET)
    public String adminHome(
            Model model,
            HttpServletRequest request,
            HttpServletResponse response) {

        Admin admin = userService.login(Admin.class, request, response);

        if (admin != null) {
            model.addAttribute("title", admin.getFullName());
            model.addAttribute("user", admin);
            model.addAttribute("admins", adminRepository.findAll());
            model.addAttribute("doctors", doctorRepository.findAll());
            return "admin";
        } else {
            return "redirect:/";
        }
    }

    @Transactional
    @RequestMapping(value="/admin/activate/{id}", method=RequestMethod.GET)
    public String activateUser(
            @PathVariable String id,
            HttpServletRequest request,
            HttpServletResponse response) {

        Admin admin = userService.login(Admin.class, request, response);
        User user = userRepository.findOne(id);

        if (admin != null &&
                user != null &&
                !user.equals(admin) &&
                user.getRole() != UserRole.PATIENT) {

            user.activate();
            userRepository.saveAndFlush(user);

            return "redirect:/admin";
        } else {
            return "redirect:/";
        }
    }

    @Transactional
    @RequestMapping(value="/admin/deactivate/{id}", method=RequestMethod.GET)
    public String deactivateUser(
            @PathVariable String id,
            HttpServletRequest request,
            HttpServletResponse response) {

        Admin admin = userService.login(Admin.class, request, response);
        User user = userRepository.findOne(id);

        if (admin != null &&
                user != null &&
                !user.equals(admin) &&
                user.getRole() != UserRole.PATIENT) {

            user.deactivate();
            userRepository.saveAndFlush(user);

            return "redirect:/admin";
        } else {
            return "redirect:/";
        }
    }
}
