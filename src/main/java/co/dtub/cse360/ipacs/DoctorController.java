package co.dtub.cse360.ipacs;

import co.dtub.cse360.ipacs.data.*;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

@Controller
public class DoctorController {

    @Autowired
    UserService userService;
    @Autowired
    PatientRepository patientRepository;
    @Autowired
    DoctorRepository doctorRepository;

    @RequestMapping(value="/doctor", method=RequestMethod.GET)
    public String doctorHome(
            Model model,
            HttpServletRequest request,
            HttpServletResponse response) {

        Doctor doctor = userService.login(Doctor.class, request, response);

        if (doctor != null) {
            model.addAttribute("title", doctor.getFullName());
            model.addAttribute("user", doctor);
            model.addAttribute("myPatients", patientRepository.findAll(PatientSpecs.doctor(doctor)));
            model.addAttribute("patients", patientRepository.findAll());
            model.addAttribute("doctors", doctorRepository.findAll());
            return "doctor";
        } else {
            return "redirect:/";
        }
    }

    @Transactional
    @RequestMapping(value="/doctor/add/{id}", method=RequestMethod.GET)
    public String addPatient(
            @PathVariable String id,
            HttpServletRequest request,
            HttpServletResponse response) {

        Doctor doctor = userService.login(Doctor.class, request, response);
        Patient patient = patientRepository.findOne(id);

        if (doctor != null && patient != null) {
            Hibernate.initialize(doctor.getPatients());
            doctor.addPatient(patient);
            doctorRepository.saveAndFlush(doctor);
            return "redirect:/doctor";
        } else {
            return "redirect:/";
        }
    }

    @Transactional
    @RequestMapping(value="/doctor/remove/{id}", method=RequestMethod.GET)
    public String removePatient(
            @PathVariable String id,
            HttpServletRequest request,
            HttpServletResponse response) {

        Doctor doctor = userService.login(Doctor.class, request, response);
        Patient patient = patientRepository.findOne(id);

        if (doctor != null && patient != null) {
            Hibernate.initialize(doctor.getPatients());
            doctor.removePatient(patient);
            doctorRepository.saveAndFlush(doctor);
            return "redirect:/doctor";
        } else {
            return "redirect:/";
        }
    }
}
