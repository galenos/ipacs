package co.dtub.cse360.ipacs;

import co.dtub.cse360.ipacs.data.*;
import co.dtub.cse360.ipacs.form.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

@Controller
public class ProfileController {

    @Autowired
    UserService userService;
    @Autowired
    UserRepository userRepository;
    @Autowired
    PatientRepository patientRepository;
    @Autowired
    DoctorRepository doctorRepository;

    @RequestMapping(value="/profile", method= RequestMethod.GET)
    public String viewProfile(
            Model model,
            HttpServletRequest request,
            HttpServletResponse response) {

        Session session = userService.login(request, response);
        if (session != null) {
            model.addAttribute("title", "Edit Profile");
            model.addAttribute("password", new PasswordForm());
            model.addAttribute("user", session.getUser());
            model.addAttribute("profile", new UserProfileForm(session.getUser(), "/profile"));
            model.addAttribute("disabled", false);
            model.addAttribute("changepassword", true);
            return "profile";
        }
        return "redirect:/";
    }

    @Transactional
    @RequestMapping(value="/profile", method=RequestMethod.POST)
    public String saveProfile(
            @ModelAttribute("profile") UserProfileForm profile,
            @ModelAttribute("password") PasswordForm password,
            Model model,
            HttpServletRequest request,
            HttpServletResponse response) {

        Session session = userService.login(request, response);
        if (session != null) {
            String message;
            message = profile.validate();
            if (!message.isEmpty()) {
                profile.setType(session.getUser().getRole().toString());
                model.addAttribute("title", "Edit Profile");
                model.addAttribute("password", new PasswordForm());
                model.addAttribute("disabled", false);
                model.addAttribute("changepassword", true);
                model.addAttribute("user", session.getUser());
                model.addAttribute("formStatus", message);
                return "profile";
            }
            if (!password.isEmpty()) {
                message = password.validate();
                if (!message.isEmpty()) {
                    model.addAttribute("title", "Edit Profile");
                    model.addAttribute("password", new PasswordForm());
                    model.addAttribute("disabled", false);
                    model.addAttribute("changepassword", true);
                    model.addAttribute("user", session.getUser());
                    model.addAttribute("formStatus", message);
                    return "profile";
                }
                password.updateUser(session.getUser());
            }
            profile.updateUser(session.getUser());
            userRepository.saveAndFlush(session.getUser());
        }
        return "redirect:/";
    }

    @RequestMapping(value="/doctors/{id}")
    public String viewDoctorProfile(
            @PathVariable String id,
            Model model,
            HttpServletRequest request,
            HttpServletResponse response) {

        Session session = userService.login(request, response);
        if (session == null || session.getUserRole() != UserRole.DOCTOR) {
            return "redirect:/";
        }
        Doctor doctor = doctorRepository.findOne(id);
        if (doctor == null) {
            return "redirect:/doctors";
        }
        model.addAttribute("title", "View " + doctor.getFullName() + "'s Doctor Profile");
        model.addAttribute("profile", new UserProfileForm(doctor, "/doctors/" + id));
        model.addAttribute("disabled", true);
        model.addAttribute("changepassword", false);
        model.addAttribute("user", session.getUser());

        return "profile";
    }
}
