package co.dtub.cse360.ipacs;

import co.dtub.cse360.ipacs.data.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class LogoutController {

    @Autowired
    UserService userService;

    @RequestMapping(value="/logout")
    public String logout(
            HttpServletRequest request,
            HttpServletResponse response) {

        userService.logout(request, response);
        return "redirect:/";
    }
}
