package co.dtub.cse360.ipacs;

import javax.transaction.Transactional;

import co.dtub.cse360.ipacs.data.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

@SpringBootApplication
public class IpacsApplication implements CommandLineRunner {

    @Autowired
    Environment environment;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AdminRepository adminRepository;

    public static void main(String[] args) {
        SpringApplication.run(IpacsApplication.class, args);
    }

    @Transactional
    @Override
    public void run(String... strings) throws Exception {

        String superEmail = environment.getProperty("ipacs.superEmail");
        String superPassword = environment.getProperty("ipacs.superPassword");

        Admin superAdmin = adminRepository.findOne(AdminSpecs.isSuper());

        if (superAdmin == null) {
            superAdmin = new Admin(superEmail, superPassword);
        } else {
            superAdmin.setEmail(superEmail);
            superAdmin.setPassword(superPassword);
        }
        userRepository.saveAndFlush(superAdmin);
    }
}
