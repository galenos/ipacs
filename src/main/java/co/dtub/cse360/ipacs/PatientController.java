package co.dtub.cse360.ipacs;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import co.dtub.cse360.ipacs.data.*;

import co.dtub.cse360.ipacs.form.MapWrapper;

import co.dtub.cse360.ipacs.form.UserProfileForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class PatientController {

    @Autowired
    private UserService userService;
    @Autowired
    private PatientRepository patientRepository;
    @Autowired
    private DoctorRepository doctorRepository;

    private SimpMessagingTemplate template;

    @Autowired
    public PatientController(SimpMessagingTemplate template) {
        this.template = template;
    }

    private void alert(Doctor doctor, String alert) {
    this.template.convertAndSend("/alerts/doctor", new AlertMessage(doctor.getId(), alert));}

    @RequestMapping(value="/patient", method=RequestMethod.GET)
    public String patientHome(
            Model model,
            HttpServletRequest request,
            HttpServletResponse response) {

        Patient patient = userService.login(Patient.class, request, response);

        if (patient != null) {
            model.addAttribute("title", patient.getFullName());
            model.addAttribute("user", patient);
            model.addAttribute("doctors", doctorRepository.findAll(DoctorSpecs.patient(patient)));
            model.addAttribute("symptoms", new MapWrapper(patient.getSymptoms(), "/patient"));
            return "patient";
        } else {
            return "redirect:/";
        }
    }

    @Transactional
    @RequestMapping(value="/patient", method=RequestMethod.POST)
    public String patientSymptoms(
            @ModelAttribute("symptoms") MapWrapper symptoms,
            Model model,
            HttpServletRequest request,
            HttpServletResponse response) {

        Patient patient = userService.login(Patient.class, request, response);

        if (patient != null) {
            patient.putSymptoms(symptoms.getMap(String.class, Integer.class));

            String severeSymptoms = patient.getSevereSymptoms();
            if (!severeSymptoms.isEmpty()) {
                for (Doctor doctor : doctorRepository.findAll(DoctorSpecs.patient(patient))) {
                    alert(doctor, "Patient <a href='/patients/" + patient.getId() + "'>" +
                            patient.getFullName() + "</a> reports severe " + severeSymptoms);
                }
            }
            patientRepository.saveAndFlush(patient);
            model.addAttribute("title", patient.getFullName());
            model.addAttribute("doctors", doctorRepository.findAll(DoctorSpecs.patient(patient)));
            model.addAttribute("user", patient);
            return "patient";
        } else {
            return "redirect:/";
        }
    }

    @RequestMapping(value="/patients/{id}", method=RequestMethod.GET)
    public String viewPatientProfile(
            @PathVariable String id,
            Model model,
            HttpServletRequest request,
            HttpServletResponse response) {

        Doctor doctor = userService.login(Doctor.class, request, response);
        Patient patient = patientRepository.findOne(id);

        if (doctor != null) {
            model.addAttribute("title", "Edit " + patient.getFullName() + "'s Patient Profile");
            model.addAttribute("profile", new UserProfileForm(patient, "/patients/" + id));
            model.addAttribute("symptoms", new MapWrapper(patient.getSymptoms(), "/patients/" + id));
            model.addAttribute("disabled", false);
            model.addAttribute("changepassword", false);
            model.addAttribute("user", doctor);

            return "patient_profile";
        } else {
            return "redirect:/";
        }
    }

    @Transactional
    @RequestMapping(value="/patients/{id}", method=RequestMethod.POST)
    public String savePatientProfile(
            @PathVariable String id,
            @ModelAttribute("profile") UserProfileForm profile,
            @ModelAttribute("symptoms") MapWrapper symptoms,
            Model model,
            HttpServletRequest request,
            HttpServletResponse response) {

        Doctor doctor = userService.login(Doctor.class, request, response);
        Patient patient = patientRepository.findOne(id);

        String profileSubmit = request.getParameter("profile_submit");
        String symptomsSubmit = request.getParameter("symptoms_submit");

        if (doctor != null && patient != null) {
            if (profileSubmit != null) {
                String message = profile.validate();
                if (!message.isEmpty()) {
                    profile.setType(patient.getRole().toString());
                    model.addAttribute("title", "Edit " + patient.getFullName() + "'s Patient Profile");
                    model.addAttribute("symptoms", new MapWrapper(patient.getSymptoms(), "/patients/" + id));
                    model.addAttribute("disabled", false);
                    model.addAttribute("changepassword", false);
                    model.addAttribute("user", doctor);
                    model.addAttribute("formStatus", message);

                    return "patient_profile";
                }
                profile.updateUser(patient);
                patientRepository.saveAndFlush(patient);
            }
            else if (symptomsSubmit != null) {
                patient.putSymptoms(symptoms.getMap(String.class, Integer.class));

                String severeSymptoms = patient.getSevereSymptoms();
                if(!severeSymptoms.isEmpty()) {
                    for(Doctor d : doctorRepository.findAll(DoctorSpecs.patient(patient))) {
                        alert(d, "Patient <a href='/patients/" + patient.getId() + "'>" +
                                patient.getFullName() + "</a> reports severe " + severeSymptoms);
                    }
                }
            }
            return "redirect:/patients/" + id;
        } else {
            return "redirect:/";
        }
    }
}
