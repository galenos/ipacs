package co.dtub.cse360.ipacs.form;

import co.dtub.cse360.ipacs.data.User;

public class PasswordForm implements Form {

    private String password;
    private String confirmPassword;

    public PasswordForm() {}

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getConfirmPassword() {
        return confirmPassword;
    }
    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public boolean isEmpty() {
        return val.string(password, confirmPassword);
    }

    public String validate() {
        if (val.password(password, confirmPassword)) {
            return "Passwords must be at least 8 characters long and match";
        }
        return "";
    }

    public void updateUser(User user) {
        user.setPassword(this.password);
    }
}
