package co.dtub.cse360.ipacs.form;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.regex.Pattern;

public interface Form {

    final class val {
        static final private Pattern emailPattern = Pattern.compile("^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]+$");

        static boolean email(String email) {
            return string(email) || !emailPattern.matcher(email).matches();
        }

        static boolean password(String pwd, String confirmPwd) {
            return string(pwd) || (pwd.length() < 8) || !pwd.equals(confirmPwd);
        }

        static boolean string(String str) {
            return str == null || str.isEmpty();
        }

        static boolean string(String... str) {
            for (String s : str) {
                if (string(s)) {
                    return true;
                }
            }
            return false;
        }
    }

    String validate();
}
