package co.dtub.cse360.ipacs.form;

import co.dtub.cse360.ipacs.data.*;

public class SignupForm implements Form {

    private String url;

    private String title;
    private String firstName;
    private String lastName;

    private String email;

    private String password;
    private String confirmPassword;

    public SignupForm() {}
    public SignupForm(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getConfirmPassword() {
        return confirmPassword;
    }
    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    @Override
    public String validate() {
        if (val.string(firstName, lastName)) {
            return "Make sure all required fields are filled in";
        }
        if (val.email(email)) {
            return "Please provide a valid email address";
        }
        if (val.password(password, confirmPassword)) {
            return "Passwords must be at least 8 characters long and match";
        }
        return "";
    }

    public Admin getAdmin() {
        return new Admin(title, firstName, lastName, email, password);
    }
    public Doctor getDoctor() {
        return new Doctor(title, firstName, lastName, email, password);
    }
    public Patient getPatient() {
        return new Patient(title, firstName, lastName, email, password);
    }
}
