package co.dtub.cse360.ipacs.form;

import co.dtub.cse360.ipacs.data.Doctor;
import co.dtub.cse360.ipacs.data.Patient;
import co.dtub.cse360.ipacs.data.User;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Calendar;

public class UserProfileForm implements Form {

    private String url;

    private String type;

    private String title;
    private String firstName;
    private String lastName;

    private String email;

    private String address;
    private String city;
    private String state;
    private String zip;

    private String primaryPhone;
    private String secondaryPhone;

    private String gender;

    private String qualifications;
    private String notes;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Calendar birthDate;

    public UserProfileForm() {}
    public UserProfileForm(User user, String url) {
        this.type = user.getRole().toString();
        this.title = user.getTitle();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.email = user.getEmail();
        this.address = user.getAddress();
        this.city = user.getCity();
        this.state = user.getState();
        this.zip = user.getZip();
        this.primaryPhone = user.getPrimaryPhone();
        this.secondaryPhone = user.getSecondaryPhone();
        this.gender = user.getGender();
        this.birthDate = user.getBirthDate();
        this.url = url;

        switch (user.getRole()) {
            case PATIENT:
                Patient patient = (Patient) user;
                this.notes = patient.getNotes();
                break;
            case DOCTOR:
                Doctor doctor = (Doctor) user;
                this.qualifications = doctor.getQualifications();
                this.notes = doctor.getNotes();
                break;
        }
    }

    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    public String getFullName() {
        String result = "";
        if (this.title != null && !this.title.isEmpty()) {
            result += this.title + ' ';
        }
        return result + this.firstName + ' ' + this.lastName;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }
    public String getZip() {
        return zip;
    }
    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getPrimaryPhone() {
        return primaryPhone;
    }
    public void setPrimaryPhone(String primaryPhone) {
        this.primaryPhone = primaryPhone;
    }
    public String getSecondaryPhone() {
        return secondaryPhone;
    }
    public void setSecondaryPhone(String secondaryPhone) {
        this.secondaryPhone = secondaryPhone;
    }

    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }

    public Calendar getBirthDate() {
        return birthDate;
    }
    public void setBirthDate(Calendar birthDate) {
        this.birthDate = birthDate;
    }

    public String getQualifications() {
        return qualifications;
    }
    public void setQualifications(String qualifications) {
        this.qualifications = qualifications;
    }
    public String getNotes() {
        return notes;
    }
    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Override
    public String validate() {
        if (val.string(firstName, lastName, address, city, state, zip, primaryPhone, gender)) {
            return "Make sure all required fields are filled in";
        }
        return "";
    }

    public void updateUser(User user) {
        user.setTitle(this.title);
        user.setFirstName(this.firstName);
        user.setLastName(this.lastName);
        user.setAddress(this.address);
        user.setCity(this.city);
        user.setState(this.state);
        user.setZip(this.zip);
        user.setPrimaryPhone(this.primaryPhone);
        user.setSecondaryPhone(this.secondaryPhone);
        user.setGender(this.gender);
        user.setBirthDate(this.birthDate);

        switch (user.getRole()) {
            case PATIENT:
                Patient patient = (Patient) user;
                patient.setNotes(this.notes);
                break;
            case DOCTOR:
                Doctor doctor = (Doctor) user;
                doctor.setQualifications(this.qualifications);
                doctor.setNotes(this.notes);
                break;
        }
    }
}
