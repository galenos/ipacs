package co.dtub.cse360.ipacs.form;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Thymeleaf doesn't like iterating maps directly, this class provides a wrapper
 * so that you can iterate maps as a property of an instance of MapWrapper.
 *
 * Furthermore, when Spring processes a form, it doesn't preserve type information
 * and defaults to type String for keys and for values. This class has a special
 * getMap(keyClass, valueClass) method that will automatically convert keys/values
 * to the desired types given that the passed classes have a static valueOf(string)
 * method. This works for all primitive class wrappers and String.
 */
public class MapWrapper {

    private String url;

    private Map<?, ?> map;

    public MapWrapper() {}
    public MapWrapper(Map<?, ?> map, String url) {
        this.map = map;
        this.url = url;
    }

    public String getUrl() {
        return this.url;
    }
    public void setUrl(String url) {
        this.url = url;
    }

    public Map<?, ?> getMap() {
        return this.map;
    }
    public <K, V> Map<K, V> getMap(Class<K> keyClass, Class<V> valueClass) {
        HashMap result = new HashMap<K, V>();
        try {
            boolean keyNotString = false, valueNotString = false;
            Method keyValueOf = null, valueValueOf = null;
            if (keyClass != String.class) {
                keyNotString = true;
                keyValueOf = keyClass.getMethod("valueOf", String.class);
            }
            if (valueClass != String.class) {
                valueNotString = true;
                valueValueOf = valueClass.getMethod("valueOf", String.class);
            }
            for (Map.Entry<?, ?> entry : this.map.entrySet()) {
                result.put(
                        keyNotString ? keyValueOf.invoke(null, entry.getKey()) : entry.getKey(),
                        valueNotString ? valueValueOf.invoke(null, entry.getValue()) : entry.getValue()
                );
            }
        } catch (ReflectiveOperationException e) {}
        return result;
    }
    public void setMap(Map<?, ?> map) {
        this.map = map;
    }
}
