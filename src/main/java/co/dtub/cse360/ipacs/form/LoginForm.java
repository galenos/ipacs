package co.dtub.cse360.ipacs.form;

public class LoginForm implements Form {
    private String email;
    private String password;
    private boolean remember;

    public LoginForm() {}

    public String getEmail() {
        return this.email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return this.password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public boolean getRemember() { return this.remember; }
    public void setRemember(boolean remember) { this.remember = remember; }

    @Override
    public String validate() {
        if (val.string(email, password)) {
            return "Incorrect login. Please try again.";
        }
        return "";
    }
}
