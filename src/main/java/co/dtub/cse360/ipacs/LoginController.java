package co.dtub.cse360.ipacs;

import co.dtub.cse360.ipacs.data.Session;
import co.dtub.cse360.ipacs.data.UserService;
import co.dtub.cse360.ipacs.form.LoginForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LoginController {

    @Autowired
    private UserService userService;

    @RequestMapping(value="/", method=RequestMethod.GET)
    public String home(
            Model model,
            HttpServletRequest request,
            HttpServletResponse response) {

        Session savedSession = userService.login(request, response);
        if (savedSession != null) {
            return loginRedirect(savedSession);
        }
        model.addAttribute("title", "Login");
        model.addAttribute("loginForm", new LoginForm());
        return "login";
    }

    @RequestMapping(value="/", method=RequestMethod.POST)
    public String login(
            @ModelAttribute("loginForm") LoginForm loginForm,
            Model model,
            HttpServletResponse response) {

        Session session = userService.login(loginForm, response);
        if (session != null) {
            return loginRedirect(session);
        }
        model.addAttribute("title", "Login");
        model.addAttribute("loginStatus", "Incorrect login. Please try again.");
        return "login";
    }

    private String loginRedirect(Session session) {
        switch (session.getUserRole()) {
            case ADMIN:
                return "redirect:/admin";
            case DOCTOR:
                return "redirect:/doctor";
            case PATIENT:
                return "redirect:/patient";
        }
        return null;
    }
}
