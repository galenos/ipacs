package co.dtub.cse360.ipacs;

import co.dtub.cse360.ipacs.data.*;
import co.dtub.cse360.ipacs.form.SignupForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

@Controller
public class AdminSignup {

    @Autowired
    UserService userService;
    @Autowired
    UserRepository userRepository;

    // Create doctor from doctor or admin view
    @RequestMapping(value="/admins/new", method= RequestMethod.GET)
    public String addAdminForm(
            Model model,
            HttpServletRequest request,
            HttpServletResponse response) {

        Session session = userService.login(request, response);
        if (session == null || session.getUserRole() != UserRole.ADMIN) {
            return "redirect:/";
        }

        model.addAttribute("title", "New Admin");
        model.addAttribute("signupForm", new SignupForm("/admins/new"));
        model.addAttribute("user", session.getUser());
        return "signup";
    }

    @Transactional
    @RequestMapping(value="/admins/new", method=RequestMethod.POST)
    public String addAdmin(
            @ModelAttribute("signupForm") SignupForm signupForm,
            Model model,
            HttpServletRequest request,
            HttpServletResponse response) {

        Session session = userService.login(request, response);
        if (session == null || session.getUserRole() != UserRole.ADMIN) {
            return "redirect:/";
        }

        String message = signupForm.validate();
        if (!message.isEmpty()) {
            model.addAttribute("title", "New Admin");
            model.addAttribute("user", session.getUser());
            model.addAttribute("formStatus", message);
            return "signup";
        }

        if (userService.exists(signupForm.getEmail())) {
            model.addAttribute("title", "New Admin");
            model.addAttribute("user", session.getUser());
            model.addAttribute("formStatus", "Email address is already in use");
            return "signup";
        }

        Admin admin = signupForm.getAdmin();
        userRepository.saveAndFlush(admin);
        return "redirect:/admin";
    }
}
